aruco_tool is a ROS package that plays a part in a larger project for a multi-robot system. The main functionality takes the image captures from ceiling-mounted cameras as inputs and outputs the poses of detected markers with respect to a referrence coordinate. The marker detection makes use of built-in functions provided in OpenCV aruco library and KL points tracker. Two executables are implemented in this packages for:

1) calibrating a camera pose with respect to a reference coordinate using a marker
   -- src/arucoCalibration.cpp

2) detecting and estimating the poses of specified markers with respect to a reference coordinate.
   -- src/arucoDetectionTopView.cpp

Core functions are implemented within .hpp files that are located in include/aruco_tool folder.

  
This package defines two messages in msg folder for encapsulating the marker poses estimated from the marker detection on the image plane.          
   -- ARMarker.msg for single marker (is not designed to be published on its own) 
   -- ARMarkers.msg for multiple markers that wraps the ARMarker.msg and has other fields including header, sequence of the image and time of image capture.  

For successful compilation with 'catkin_make', this package requires dependancies on OpenCV3 and other ROS packages (developed for the proect) for the message types (e.g. ImageSync, which is used for synchronised image capturing among cameras on multiple machines). Compilation of this single package will fail if dependencies are not provided.